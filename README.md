# Install pre-compiled binaries

## Homebrew

```
brew install chriz2600/tap/xhdmi-tools
```

## Scoop

```
scoop bucket add chriz2600 https://gitlab.com/chriz2600/scoop-bucket.git
scoop install xhdmi-tools
```

## deb/rpm

Download the `.deb` or `.rpm` from the [releases page](https://gitlab.com/chriz2600/xhdmi-tools/-/releases) and install with `dpkg -i` and `rpm -i` respectively.

## Manual install

Download pre-compiled binaries from the [releases page](https://gitlab.com/chriz2600/xhdmi-tools/-/releases) and copy to the desired location.

<!-- # Running with Docker

docker run --rm  -->

# Setup

First `add` a `console` to the config, so you don't have to enter hostname/user and password every time running a command. It's also necessary to enter a "HTTP Password" during web console `setup`.

```
xhdmi-tools console add NAME --hostname HOSTNAME --user USER
```
```
NAME:     Name of the console
HOSTNAME: IP adress or hostname of the console 
          The IP adress can be found in the OSD (Main Menu->WiFi Setup)
USER:     Username ("HTTP User" in setup, "Web login username" in OSD WiFi Setup)

The tool will prompt for the password ("HTTP Password" in web console setup, "Web login password" in OSD WiFi Setup)
```
If this is the first `console` added, it will also be selected as `active`, otherwise use:
```
xhdmi-tools console use <name-you-choose>
```

---

# Manual firmware upgrade

## Prerequisites

Refer to [here](#setup)

## Procedure

1. **Download firmware files**
    e.g.
    ```
    wget "https://firmware.i74.de/ps1/master/fpga-firmware.rpd"
    wget "https://firmware.i74.de/ps1/master/esp-firmware.bin"
    wget "https://firmware.i74.de/ps1/master/esp.index.html.gz"
    ```

2. **Upload firmware files to console**
    ```
    xhdmi-tools console fw upload fpga-firmware.rpd fpga
    xhdmi-tools console fw upload esp-firmware.bin esp
    xhdmi-tools console fw upload esp.index.html.gz index
    ```

3. **Flash firmware**
    ```
    xhdmi-tools console fw flash
    ```

4. **Restart with new firmware**
    ```
    xhdmi-tools console reset
    ```

---

# Quick guide to fonts/character maps

## Prerequisites

Refer to [here](#setup)

## Procedure

1. **Get the default font image with:**
    ```
    xhdmi-tools font getdefault DEFAULT.png
    ```

    The `default.png` can also be found here: [testdata/default.png](https://gitlab.com/chriz2600/xhdmi-tools/-/blob/master/testdata/default.png).

2. **Edit the font image with an image editor of choice and save the result to a PNG.**

    - Characters are in ASCII order

    - Each character is 8x16 pixels

    - There is no spacing between characters

    - Max ASCII value is 0xBF.
    
    - Characters >= 0x80 are used to create the logo.

3. **After editing, you can create the data file with:**
    ```
    xhdmi-tools font fromimage INPUTFILE.png OUTPUTFILE.data
    ```

4. **This file can then be uploaded to the console:**
    ```
    xhdmi-tools font upload INPUTFILE.data OSDNAME
    ```

5. **The font can be shown on the console by using:**
    ```
    xhdmi-tools font show OSDNAME
    ```
    This also opens the OSD.
    <br/>You can also select the font from the OSD manually (`Main Menu->OSD Settings`)

---

# Quick guide to gamma/color maps

## Prerequisites

Refer to [here](#setup)

## Procedure

Gamma/color maps are binary files (`xhdmi-tools` provides commands to convert these into CSV files.) stored on PS1Digital which map incoming pixel data to a new value.
<br>Once uploaded they are automatically added to the OSD selection.

1. **Create a gamma map for a given gamma value / Download an existing gamma/color map:**
    ```
    xhdmi-tools gamma createcsv 1.0 OUTPUT.csv
    ```
    This will create a colormap csv `OUTPUT.csv` for a gamma value of `1.0`.

    Or download an existing gamma/color map from your console (and convert it into csv format):
    ```
    xhdmi-tools gamma download NAME - | xhdmi-tools gamma tocsv - OUTPUT.csv
    ```

    ```
    xhdmi-tools gamma list
    ```
    shows all currently installed gamma maps on the console (an * marks the active map)

2. **Edit the gamma/color map**

    - The first column refers to the input value of the respective color

    - The 2nd column is the **red** output value, the 3rd column is **green** and the 4th is **blue**

3. **After editing, you can create the binary data file with:**
    ```
    xhdmi-tools gamma fromcsv EDIT.csv EDIT.data
    ```

4. **Upload this file to the console:**
    ```
    xhdmi-tools gamma upload EDIT.data OSDNAME
    ```

5. **Show the map on the console by using:**
    ```
    xhdmi-tools gamma show OSDNAME
    ```
    You can also select the font from the OSD manually (`Main Menu->Advanced Video Settings->Gamma adjust`)

---

## Notes

- All commands involving files accept the '-' for stdin/stdout, so commands can be chained together by pipelining, e.g.:

    ```
    xhdmi-tools font fromimage EDITED.png - | xhdmi-tools font upload - OSDNAME
    ```

- `xhdmi-tools` shows available commands and context sensitive help:

    Examples:

    ```
    $ xhdmi-tools
    XHDMI tools (version 1.2.1)
    API tools for XHDMI based modkits, e.g. PS1Digital

    Usage:
    xhdmi-tools [command]

    Available Commands:
    completion  Generates bash/zsh completion
    config      Manage console config
    console     Manage consoles
    font        Handles XHDMI font definition files
    gamma       Handles XHDMI gamma maps
    version     Print xhdmi-tools version
    help        Help about any command

    Flags:
    -h, --help              help for xhdmi-tools
        --hostname string   hostname
        --password string   password
        --user string       user

    Use "xhdmi-tools [command] --help" for more information about a command.
    ```
    ```
    $ xhdmi-tools console
    A console is represented by a user defined NAME and a HOSTNAME/USER/PASSWORD combination.

    Usage:
    xhdmi-tools console [command]

    Available Commands:
    add         Adds a console
    remove      Removes a console
    list        List consoles
    use         Set the active console
    reset       Reset the active console
    cleanup     Removes all previously staged firmware
    fw          Console firmware functions
    testdata    Get testdata from console (ALPHA!)
    sysinfo     Get sysinfo from console (ALPHA!)
    ping        Ping console

    Flags:
    -h, --help   help for console

    Global Flags:
        --hostname string   hostname
        --password string   password
        --user string       user

    Use "xhdmi-tools console [command] --help" for more information about a command.
    ```
    ```
    $ xhdmi-tools console add
    Error: accepts 1 arg(s), received 0
    Usage:
    xhdmi-tools console add NAME [flags]

    Flags:
    -h, --help   help for add

    Global Flags:
        --hostname string   hostname
        --password string   password
        --user string       user
    ```