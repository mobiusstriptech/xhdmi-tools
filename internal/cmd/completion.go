package cmd

import (
	"errors"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

// completionCmd represents the completion command
var completionCmd = &cobra.Command{
	Use:   "completion TYPE",
	Short: "Generates bash/zsh completion",
	Long: `Use:
  ZSH: mkdir -p ~/.oh-my-zsh/completions/ && xhdmi-tools completion zsh > ~/.oh-my-zsh/completions/_xhdmi-tools
    or
  BASH: source <(xhdmi-tools completion bash)`,
	Args: cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		completionType := strings.TrimSpace(args[0])

		if completionType != "zsh" && completionType != "bash" {
			return errors.New("Only 'bash' or 'zsh' are supported at the moment")
		}

		if completionType == "zsh" {
			rootCmd.GenZshCompletion(os.Stdout)
		} else if completionType == "bash" {
			rootCmd.GenBashCompletion(os.Stdout)
		}
		return nil
	},
}

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(completionCmd)
	completionCmd.MarkZshCompPositionalArgumentWords(1, "bash", "zsh")
}
