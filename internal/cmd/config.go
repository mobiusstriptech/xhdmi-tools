package cmd

import (
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

var (
	configCmd = &cobra.Command{
		Use:   "config",
		Short: "Manage console config",
		Long:  `Downloads and uploads the console config file`,
	}

	configDownloadCmd = &cobra.Command{
		Use:   "download FILE",
		Short: "Get the config from the console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.DownloadConfig(filename)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully downloaded config to %s\n", filename)
			}
			return err
		},
	}

	configUploadCmd = &cobra.Command{
		Use:   "upload FILE",
		Short: "Uploads a config to the console",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			filename := strings.TrimSpace(args[0])
			console, err := getCurrentConsole()
			if err != nil {
				return err
			}
			err = console.UploadConfig(filename)
			if err == nil {
				fmt.Fprintf(os.Stderr, "Successfully uploaded config from %s\n", filename)
			}
			return err
		},
	}
)

func init() {
	cobra.EnableCommandSorting = false
	rootCmd.AddCommand(configCmd)
	configCmd.AddCommand(configDownloadCmd)
	configCmd.AddCommand(configUploadCmd)
	configDownloadCmd.MarkZshCompPositionalArgumentFile(1)
	configUploadCmd.MarkZshCompPositionalArgumentFile(1)
}
