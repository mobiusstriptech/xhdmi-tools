package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	dac "github.com/xinsnake/go-http-digest-auth-client"
	"gopkg.in/yaml.v3"
)

func (console *Console) DownloadConfig(filename string) error {
	var (
		writer *os.File
		err    error
	)

	writer = os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	reqURL := console.CreateRequestUrl("/config.json", nil)
	// req, err := http.NewRequest("GET", reqURL.String(), nil)
	// if err != nil {
	// 	return err
	// }
	req := dac.NewRequest(console.User, console.Password, "GET", reqURL.String(), "")
	resp, err := req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("download config failed: %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	//writer.Write(body)

	result := Configuration{}

	// HACK: defaults
	result.Scanlines.Dopre = true

	json.Unmarshal(body, &result)

	fmt.Fprintf(writer, "# See psx.i74.de/config for config documentation\n")
	yamlEnc := yaml.NewEncoder(writer)
	yamlEnc.SetIndent(2)
	err = yamlEnc.Encode(result)
	yamlEnc.Close()

	// x, _ := json.MarshalIndent(result, "", "  ")
	// writer.Write(x)

	if err != nil {
		return err
	}

	return nil
}

func (console *Console) UploadConfig(filename string) error {
	var (
		reader *os.File
		resp   *http.Response
		err    error
	)

	reader = os.Stdin
	if filename != "" && filename != "-" {
		reader, err = os.Open(filename)

		if err != nil {
			return err
		}
		defer reader.Close()
	}

	result := Configuration{}
	yamlDec := yaml.NewDecoder(reader)
	err = yamlDec.Decode(&result)
	if err != nil {
		return err
	}

	data, err := json.Marshal(result)

	//fmt.Printf("%s\n", data)

	reqURL := console.CreateRequestUrl("/upload/config", nil)
	req, err := console.createMultipartRequest(reqURL, bytes.NewReader(data), nil)
	if err != nil {
		return err
	}

	resp, err = req.Execute()
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return fmt.Errorf("Upload config failed: %d", resp.StatusCode)
	}
	return nil
}

type FWRequiredAction int

const (
	FWDownload FWRequiredAction = 0
	FWFlash    FWRequiredAction = 1
	FWOK       FWRequiredAction = 2
)

type FWCheckResults struct {
	Action    FWRequiredAction
	Installed string
	Staged    string
	Server    string
}

type FWCheck struct {
	FPGA            FWCheckResults
	ESP             FWCheckResults
	Index           FWCheckResults
	FirmwareServer  string
	FirmwarePath    string
	FirmwareVersion string
}

func (console *Console) CheckForFirmware() (*FWCheck, error) {
	out := make(chan MD5ChecksumResponse)

	result := FWCheck{}
	resCount := 9

	go console.RequestMD5Checksum(out, "/etc/last_flash_svf_md5", &result.FPGA.Installed)
	go console.RequestMD5Checksum(out, "/etc/last_esp_flash_md5", &result.ESP.Installed)
	go console.RequestMD5Checksum(out, "/index.html.gz.md5", &result.Index.Installed)
	go console.RequestMD5Checksum(out, "/fpga-firmware.rpd.md5", &result.FPGA.Staged)
	go console.RequestMD5Checksum(out, "/esp-firmware.bin.md5", &result.ESP.Staged)
	go console.RequestMD5Checksum(out, "/esp.index.html.gz.md5", &result.Index.Staged)

	ac := ActiveConsoleConfig{}
	err := console.List(&ac, "/config", nil)
	if err != nil {
		return nil, err
	}

	result.FirmwareServer = ac.FirmwareServer
	result.FirmwarePath = ac.FirmwarePath
	result.FirmwareVersion = ac.FirmwareVersion

	go RequestMD5Checksum(out, fmt.Sprintf("https://%s%s/%s/fpga-firmware.rpd.md5", ac.FirmwareServer, ac.FirmwarePath, ac.FirmwareVersion), &result.FPGA.Server, nil)
	go RequestMD5Checksum(out, fmt.Sprintf("https://%s%s/%s/esp-firmware.bin.md5", ac.FirmwareServer, ac.FirmwarePath, ac.FirmwareVersion), &result.ESP.Server, nil)
	go RequestMD5Checksum(out, fmt.Sprintf("https://%s%s/%s/esp.index.html.gz.md5", ac.FirmwareServer, ac.FirmwarePath, ac.FirmwareVersion), &result.Index.Server, nil)

	i := 0
	for res := range out {
		i = i + 1
		if res.Err != nil {
			return nil, res.Err
		}
		if i >= resCount {
			break
		}
	}

	close(out)

	result.FPGA.Action = FWDownload
	result.ESP.Action = FWDownload
	result.Index.Action = FWDownload

	if result.FPGA.Installed == result.FPGA.Server {
		result.FPGA.Action = FWOK
	} else if result.FPGA.Staged == result.FPGA.Server {
		result.FPGA.Action = FWFlash
	}
	if result.ESP.Installed == result.ESP.Server {
		result.ESP.Action = FWOK
	} else if result.ESP.Staged == result.ESP.Server {
		result.ESP.Action = FWFlash
	}
	if result.Index.Installed == result.Index.Server {
		result.Index.Action = FWOK
	} else if result.Index.Staged == result.Index.Server {
		result.Index.Action = FWFlash
	}

	return &result, nil
}
