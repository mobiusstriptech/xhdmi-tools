package api

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func GammaCalc(original int, gamma float64) int {
	return int(math.Pow((float64(original)/255), gamma) * 255)
}

func GammaMapCSV(output string, gamma float64) error {
	var (
		err    error
		writer *os.File
	)

	writer = os.Stdout
	if output != "" && output != "-" {
		writer, err = os.OpenFile(output, os.O_RDWR|os.O_CREATE|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	GammaMapCSVToWriter(writer, gamma)
	return nil
}

func GammaMapCSVToWriter(writer io.Writer, gamma float64) {
	fmt.Fprintf(writer, "# gamma map %f (1 / %f)\n", gamma, 1/gamma)
	for val := 0; val < 256; val++ {
		gval := GammaCalc(val, gamma)
		fmt.Fprintf(writer, "%d,%d,%d,%d\n", val, gval, gval, gval)
	}
}

func GammaCSVToData(inputName, outputName string) error {
	var (
		err    error
		reader *os.File
		writer *os.File
	)

	reader = os.Stdin
	if inputName != "" && inputName != "-" {
		reader, err = os.Open(inputName)
		if err != nil {
			return err
		}
		defer reader.Close()
	}

	writer = os.Stdout
	if outputName != "" && outputName != "-" {
		writer, err = os.OpenFile(outputName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	GammaCSVToDataIO(reader, writer)
	return nil
}

func GammaCSVToDataIO(reader io.Reader, writer io.Writer) {
	redMap := make([]byte, 256, 256)
	greenMap := make([]byte, 256, 256)
	blueMap := make([]byte, 256, 256)

	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		s := scanner.Text()
		m, _ := regexp.Match("^\\s*#", []byte(s))
		if m {
			continue
		}
		values := strings.Split(s, ",")
		if len(values) >= 4 {
			inVal, _ := strconv.ParseUint(values[0], 10, 8)
			rv, _ := strconv.ParseUint(values[1], 10, 8)
			gv, _ := strconv.ParseUint(values[2], 10, 8)
			bv, _ := strconv.ParseUint(values[3], 10, 8)

			redMap[inVal] = byte(rv)
			greenMap[inVal] = byte(gv)
			blueMap[inVal] = byte(bv)
		}
	}

	writer.Write(redMap)
	writer.Write(greenMap)
	writer.Write(blueMap)
}

func CreateCSVFromGammaData(inputName, outputName string) error {
	var (
		err    error
		reader *os.File
		writer *os.File
	)

	reader = os.Stdin
	if inputName != "" && inputName != "-" {
		reader, err = os.Open(inputName)
		if err != nil {
			return err
		}
		defer reader.Close()
	}

	writer = os.Stdout
	if outputName != "" && outputName != "-" {
		writer, err = os.OpenFile(outputName, os.O_RDWR|os.O_CREATE|os.O_TRUNC, os.FileMode(0644))
		if err != nil {
			return err
		}
		defer writer.Close()
	}

	redMap := make([]byte, 256, 256)
	greenMap := make([]byte, 256, 256)
	blueMap := make([]byte, 256, 256)

	len, err := reader.Read(redMap)
	if err != nil || len != 256 {
		return errors.New("reading readMap failed")
	}
	len, err = reader.Read(greenMap)
	if err != nil || len != 256 {
		return errors.New("reading greenMap failed")
	}
	len, err = reader.Read(blueMap)
	if err != nil || len != 256 {
		return errors.New("reading blueMap failed")
	}

	fmt.Fprintf(writer, "# gamma map from file %s\n", inputName)
	for val := 0; val < 256; val++ {
		fmt.Fprintf(writer, "%d,%d,%d,%d\n", val, redMap[val], greenMap[val], blueMap[val])
	}

	return nil
}

func (console *Console) DownloadGammaMap(name string, filename string) error {
	path := fmt.Sprintf("/gdata/%s.gdata", name)
	fmt.Fprintf(os.Stderr, "%s\n", path)
	return console.Download(path, filename)
}

func (console *Console) UploadGammaMap(filename string, name string) error {
	return console.Upload(filename, "/upload/gamma", map[string]string{"name": name})
}

func (console *Console) ListGammaMaps() (*GammaList, error) {
	result := GammaList{}
	err := console.List(&result, "/gamma/list", nil)
	return &result, err
}

func (console *Console) DeleteGammaMap(name string) error {
	return console.Invoke("/delete/gamma", map[string]string{"name": name})
}

func (console *Console) LoadGammaMap(name string) error {
	return console.Invoke("/gamma/load", map[string]string{"name": name})
}
