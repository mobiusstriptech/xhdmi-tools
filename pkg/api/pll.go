package api

import (
	"errors"
	"fmt"
	"math/big"
	"os"
	"text/tabwriter"
	"time"

	"gopkg.in/yaml.v3"
)

type PLLNumber byte
type ClockNumber byte
type IDrv byte

const (
	RegOEB    = 3
	RegPllSrc = 15
	RegXtalCl = 183
)

const (
	ClockPdnDown byte = 1 << 7
	ClockPdnUp   byte = 0
	ClockSrcMS   byte = 3 << 2
	PllASrcClkIn byte = 1 << 2
	PllBSrcClkIn byte = 1 << 3
	XtalCl6pf    byte = 1 << 6
	XtalCl8pf    byte = 2 << 6
	XtalCl10pf   byte = 3 << 6
	XtalReserved byte = 0x12
)

const (
	PLLA PLLNumber = 0
	PLLB PLLNumber = 1
)

const (
	Clock0 ClockNumber = 0
	Clock1 ClockNumber = 1
	Clock2 ClockNumber = 2
	Clock3 ClockNumber = 3
	Clock4 ClockNumber = 4
	Clock5 ClockNumber = 5
	Clock6 ClockNumber = 6
	Clock7 ClockNumber = 7
)

const (
	IDrv2ma IDrv = 0
	IDrv4ma IDrv = 1
	IDrv6ma IDrv = 2
	IDrv8ma IDrv = 3
)

type PLLParameter struct {
	MSNxP1 int64
	MSNxP2 int64
	MSNxP3 int64
}

type ClockParameter struct {
	MSxP1  int64
	MSxP2  int64
	MSxP3  int64
	DivBy4 bool
}

type PLL struct {
	Number      PLLNumber
	FreqClock   *big.Rat
	FreqRef     *big.Rat
	N           *big.Rat
	M           *big.Rat
	FreqVCO     *big.Rat
	UseClockIn  bool
	Params      *PLLParameter
	BaseAddress byte
	PllName     string
}

type Clock struct {
	Number       ClockNumber
	PLL          *PLL
	FreqOut      *big.Rat
	R            *big.Rat
	N            *big.Rat
	IsIntDivider bool
	IDrv         IDrv
	Params       *ClockParameter
	BaseAddress  byte
}

type reg struct {
	v byte
}

type registerMap struct {
	head []*reg
	main []*reg
	tail []*reg
}

type FrequencyPlan struct {
	FreqSource  *big.Rat
	FreqBase    *big.Rat
	P           int64
	XtalCL      byte
	PLLa        *PLL
	PLLb        *PLL
	Clocks      []*Clock
	registerMap registerMap
}

func NewClock(clockNumber ClockNumber, pll *PLL, fout *big.Rat, idrv IDrv) (*Clock, error) {
	clock := Clock{
		Number:  clockNumber,
		PLL:     pll,
		FreqOut: fout,
		IDrv:    idrv,
		R:       From_int(1),
	}
	if clockNumber < 6 {
		clock.BaseAddress = byte(42 + (clockNumber * 8))
	} else {
		clock.BaseAddress = byte(42 + (6 * 8) + clockNumber - 6)
	}

	if clock.FreqOut.Cmp(clock.PLL.FreqRef) == 0 {
		clock.N = clock.PLL.N
	} else {
		clock.N = new(big.Rat).Quo(clock.PLL.FreqVCO, clock.FreqOut)
	}

	if !clock.isValidN() {
		return nil, func(i ...int64) error { return fmt.Errorf("Invalid value for clock.N: %d + %d/%d", i[0], i[1], i[2]) }(To_abc(clock.N))
	}

	clock.IsIntDivider = clock.Number < 6 && clock.N.IsInt()
	err := clock.calcParameters()
	if err != nil {
		return nil, err
	}

	return &clock, nil
}

func (clock *Clock) isValidN() bool {
	if clock.N.IsInt() {
		return true
	}
	a, _, c := To_abc(clock.N)
	if a < 8 || a > 900 {
		return false
	}
	if c > 1048575 {
		return false
	}
	return true
}

func (clock *Clock) calcParameters() error {

	params := ClockParameter{}

	if clock.N.IsInt() && clock.N.Num().Int64() == 4 {
		params.MSxP1 = 0
		params.MSxP2 = 0
		params.MSxP3 = 1
		params.DivBy4 = true
		clock.Params = &params
		return nil
	}

	if clock.Number < 6 {
		a, b, c := To_abc(clock.N)

		params.MSxP1 = 128*a + (128 * b / c) - 512
		params.MSxP2 = 128*b - c*(128*b/c)
		params.MSxP3 = c
		params.DivBy4 = false
		clock.Params = &params

		return nil
	}

	if clock.N.IsInt() {
		params.MSxP1 = clock.N.Num().Int64()
		params.DivBy4 = false
		clock.Params = &params
		return nil
	}

	return errors.New("Could not create clock params")
}

func (clock *Clock) debug(writer *os.File) {
	w := tabwriter.NewWriter(writer, 0, 1, 1, ' ', tabwriter.AlignRight)
	fmt.Fprintf(w, "* \tClock: \t%6d\n", clock.Number)
	fmt.Fprintf(w, "* \tBaseAddress: \t  0x%02X\n", clock.BaseAddress)
	func(i ...int64) { fmt.Fprintf(w, "* \tFreqOut: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(clock.FreqOut))
	func(i ...int64) { fmt.Fprintf(w, "* \tR: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(clock.R))
	func(i ...int64) { fmt.Fprintf(w, "* \tN: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(clock.N))
	fmt.Fprintf(w, "* \tPLL: \t%6s\n", clock.PLL.PllName)
	fmt.Fprintf(w, "* \tisIntDivider: \t%6t\n", clock.IsIntDivider)
	fmt.Fprintf(w, "* \tmax current: \t%6d mA\n", (clock.IDrv+1)*2)
	if clock.Params != nil {
		fmt.Fprintf(w, "* \tMSN%d_P1: \t%6d\n", clock.Number, clock.Params.MSxP1)
		fmt.Fprintf(w, "* \tMSN%d_P2: \t%6d\n", clock.Number, clock.Params.MSxP2)
		fmt.Fprintf(w, "* \tMSN%d_P3: \t%6d\n", clock.Number, clock.Params.MSxP3)
		fmt.Fprintf(w, "* \tDivBy4: \t%6t\n* \t\n", clock.Params.DivBy4)
	}
	w.Flush()
}

func NewPLL(pllNumber PLLNumber, fclk *big.Rat, fref *big.Rat, useclkin bool) (*PLL, error) {
	pll := PLL{
		Number:     pllNumber,
		FreqClock:  fclk,
		FreqRef:    fref,
		UseClockIn: useclkin,
	}
	if pllNumber == PLLA {
		pll.BaseAddress = 26
		pll.PllName = "A"
	} else {
		pll.BaseAddress = 34
		pll.PllName = "B"
	}
	err := pll.calcPll()
	if err != nil {
		return nil, err
	}
	err = pll.calcParameters()

	return &pll, nil
}

func (pll *PLL) calcParameters() error {
	a, b, c := To_abc(pll.M)

	params := PLLParameter{}

	params.MSNxP1 = 128*a + (128 * b / c) - 512
	params.MSNxP2 = 128*b - c*(128*b/c)
	params.MSNxP3 = c

	pll.Params = &params
	return nil
}

func isValidM(m *big.Rat) bool {
	a, _, c := To_abc(m)
	if a < 15 || a > 90 {
		return false
	}
	if c > 1048575 {
		return false
	}
	return true
}

func (pll *PLL) setMandN(mult *big.Rat) error {
	pll.N = mult
	pll.M = new(big.Rat).Quo(new(big.Rat).Mul(pll.FreqRef, mult), pll.FreqClock)
	if !isValidM(pll.M) {
		return func(i ...int64) error { return fmt.Errorf("Invalid value for pll.M: %d + %d/%d", i[0], i[1], i[2]) }(To_abc(pll.M))
	}
	return nil
}

func isValidVCOFreq(fvco *big.Rat) bool {
	return fvco.Cmp(big.NewRat(600, 1)) > 0 && fvco.Cmp(big.NewRat(900, 1)) < 0
}

func (pll *PLL) calcPll() error {
	if pll.FreqRef.Cmp(big.NewRat(150, 1)) > 0 {
		mult := big.NewRat(4, 1)
		pll.FreqVCO = new(big.Rat).Mul(pll.FreqRef, mult)
		return pll.setMandN(mult)
	}
	mults := [...]int64{8, 6, 4}
	for _mult := range mults {
		mult := big.NewRat(mults[_mult], 1)
		fvco := new(big.Rat).Mul(pll.FreqRef, mult)
		if isValidVCOFreq(fvco) {
			pll.FreqVCO = fvco
			return pll.setMandN(mult)
		}
	}
	_mult := int64(900)
	for {
		mult := big.NewRat(_mult, 1)
		fvco := new(big.Rat).Mul(pll.FreqRef, mult)
		if isValidVCOFreq(fvco) {
			pll.FreqVCO = fvco
			return pll.setMandN(mult)
		}
		_mult = _mult - 1
		if _mult == 8 {
			break
		}
	}
	return errors.New("Could not find FreqVCO")
}

func (pll *PLL) debug(writer *os.File) {
	w := tabwriter.NewWriter(writer, 0, 1, 1, ' ', tabwriter.AlignRight)
	fmt.Fprintf(w, "* \tPLL: \t%6s\n", pll.PllName)
	fmt.Fprintf(w, "* \tBaseAddress: \t  0x%02X\n", pll.BaseAddress)
	func(i ...int64) { fmt.Fprintf(w, "* \tFreqClock: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(pll.FreqClock))
	func(i ...int64) { fmt.Fprintf(w, "* \tFreqRef: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(pll.FreqRef))
	func(i ...int64) { fmt.Fprintf(w, "* \tM: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(pll.M))
	func(i ...int64) { fmt.Fprintf(w, "* \tFreqVCO: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(pll.FreqVCO))
	func(i ...int64) { fmt.Fprintf(w, "* \tFreqRef: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(pll.FreqRef))
	uci := "XO"
	if pll.UseClockIn {
		uci = "CLK_IN"
	}
	fmt.Fprintf(w, "* \t  Clk source: \t%6s\n", uci)
	if pll.Params != nil {
		fmt.Fprintf(w, "* \tMSN%s_P1: \t%6d\n", pll.PllName, pll.Params.MSNxP1)
		fmt.Fprintf(w, "* \tMSN%s_P2: \t%6d\n", pll.PllName, pll.Params.MSNxP2)
		fmt.Fprintf(w, "* \tMSN%s_P3: \t%6d\n* \t\n", pll.PllName, pll.Params.MSNxP3)
	}
	w.Flush()
}

func NewFrequencyPlan(fin *big.Rat, xtalcl byte) (*FrequencyPlan, error) {
	plan := FrequencyPlan{FreqSource: fin}

	for _, v := range []int64{1, 2, 4, 8} {
		fb := new(big.Rat).Quo(plan.FreqSource, From_int(v))
		if fb.Cmp(From_int(10)) >= 0 && fb.Cmp(From_int(40)) <= 0 {
			plan.FreqBase = fb
			plan.P = v
			break
		}
	}

	if plan.FreqBase == nil {
		return nil, errors.New("Source clock invalid")
	}

	plan.Clocks = make([]*Clock, 8)
	plan.XtalCL = xtalcl
	return &plan, nil
}

func mapClkInDivider(value int64) byte {
	switch value {
	case 1:
		return 0b00 << 6
	case 2:
		return 0b01 << 6
	case 4:
		return 0b10 << 6
	case 8:
		return 0b11 << 6
	}
	return 0b00 << 6
}

func (plan *FrequencyPlan) prepareRegisters(skipHead bool) int {
	plan.registerMap = registerMap{}
	plan.registerMap.head = make([]*reg, 256)
	plan.registerMap.main = make([]*reg, 256)
	plan.registerMap.tail = make([]*reg, 256)
	if !skipHead {
		plan.fillHead()
	}
	plan.fillMain()
	plan.fillTail()
	return plan.countRegs()
}

func (plan *FrequencyPlan) WritePLLTune(name string, filename string) error {
	var (
		writer *os.File
		err    error
	)

	writer = os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))

		if err != nil {
			return err
		}
		defer writer.Close()
	}

	plan.prepareRegisters(true)

	i := 0x1A
	j := 0
	fmt.Fprintf(writer, "`define %s { ", name)
	for j < 16 {
		komma := ","
		if j == 16-1 {
			komma = ""
		}
		fmt.Fprintf(writer, "8'h_%02X%s ", plan.registerMap.main[i].v, komma)
		i++
		j++
	}
	fmt.Fprintf(writer, "}\n")

	return nil
}

func (plan *FrequencyPlan) WriteVerilogInclude(taskname string, filename string, skipHead bool, addTsAndVer bool) error {
	var (
		writer *os.File
		err    error
	)

	writer = os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))

		if err != nil {
			return err
		}
		defer writer.Close()
	}

	total := plan.prepareRegisters(skipHead)
	count := 0
	plan.writeVerilogIncludeHeader(writer, taskname, count, addTsAndVer)
	count = writeVerilogIncludeRegs(writer, plan.registerMap.head, count)
	count = writeVerilogIncludeRegs(writer, plan.registerMap.main, count)
	count = writeVerilogIncludeRegs(writer, plan.registerMap.tail, count)
	plan.writeVerilogIncludeFooter(writer)
	//
	if count != total {
		return errors.New("ERROR: register length mismatch")
	}
	return nil
}

func (plan *FrequencyPlan) writeVerilogIncludeHeader(writer *os.File, taskname string, count int, addTsAndVer bool) error {
	now := time.Now()
	if addTsAndVer {
		fmt.Fprintf(writer, "// Created by xhdmi-tools (%s): %s\n", Version, now.String())
	}
	fmt.Fprintf(writer, "`ifndef %s\n", taskname)
	fmt.Fprintf(writer, "`define %s\n", taskname)
	fmt.Fprintf(writer, "task %s;\n", taskname)
	fmt.Fprintf(writer, "   input [2:0] next_cmd;\n\n")
	fmt.Fprintf(writer, "   begin\n")
	fmt.Fprintf(writer, "       case (subcmd_counter)\n")
	return nil
}

func (plan *FrequencyPlan) writeVerilogIncludeFooter(writer *os.File) {
	fmt.Fprintf(writer, "            default: begin\n")
	fmt.Fprintf(writer, "                cmd_counter <= next_cmd;\n")
	fmt.Fprintf(writer, "                subcmd_counter <= scs_start;\n")
	fmt.Fprintf(writer, "            end\n")
	fmt.Fprintf(writer, "        endcase\n")
	fmt.Fprintf(writer, "    end\n")
	fmt.Fprintf(writer, "endtask\n")
	fmt.Fprintf(writer, "`endif\n")
}

func writeVerilogIncludeRegs(writer *os.File, regs []*reg, start int) int {
	count := start
	for i, r := range regs {
		if r != nil {
			if i == 0xA6 {
				fmt.Fprintf(writer, "            %d: write_i2c(CHIP_ADDR, { 8'h_%02X, clock_config.clock1 });\n", count, i)
			} else if i == 0xAA {
				fmt.Fprintf(writer, "            %d: write_i2c(CHIP_ADDR, { 8'h_%02X, clock_config.clock5 });\n", count, i)
			} else {
				fmt.Fprintf(writer, "            %d: write_i2c(CHIP_ADDR, 16'h_%02X_%02X);\n", count, i, r.v)
			}
			count = count + 1
		}
	}
	return count
}

func (plan *FrequencyPlan) WriteHeaderFile(filename string, skipHead bool, addTsAndVer bool) error {
	var (
		writer *os.File
		err    error
	)

	writer = os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))

		if err != nil {
			return err
		}
		defer writer.Close()
	}

	count := plan.prepareRegisters(skipHead)
	plan.writeHeaderFileHeader(writer, count, addTsAndVer)
	writeHeaderFileRegs(writer, plan.registerMap.head)
	writeHeaderFileRegs(writer, plan.registerMap.main)
	writeHeaderFileRegs(writer, plan.registerMap.tail)
	plan.writeHeaderFileFooter(writer)

	return nil
}

func (plan *FrequencyPlan) countRegs() int {
	count := 0
	for _, r := range plan.registerMap.head {
		if r != nil {
			count = count + 1
		}
	}
	for _, r := range plan.registerMap.main {
		if r != nil {
			count = count + 1
		}
	}
	for _, r := range plan.registerMap.tail {
		if r != nil {
			count = count + 1
		}
	}
	return count
}

func (plan *FrequencyPlan) writeHeaderFileHeader(writer *os.File, count int, addTsAndVer bool) {
	now := time.Now()
	if addTsAndVer {
		fmt.Fprintf(writer, "/*\n")
		fmt.Fprintf(writer, " *  Created by xhdmi-tools (%s)\n", Version)
		fmt.Fprintf(writer, " *    Timestamp: %s\n", now.String())
		fmt.Fprintf(writer, " */\n\n")
	}
	fmt.Fprintf(writer, "#ifndef SI5351C_REVB_REG_CONFIG_HEADER\n#define SI5351C_REVB_REG_CONFIG_HEADER\n\n")
	fmt.Fprintf(writer, "#define SI5351C_REVB_REG_CONFIG_NUM_REGS %d\n\n", count)
	fmt.Fprintf(writer, "typedef struct\n{\n    unsigned int address; /* 16-bit register address */\n    unsigned char value; /* 8-bit register data */\n\n} si5351c_revb_register_t;\n\n")
	fmt.Fprintf(writer, "si5351c_revb_register_t const si5351c_revb_registers[SI5351C_REVB_REG_CONFIG_NUM_REGS] =\n{\n")
}

func (plan *FrequencyPlan) writeHeaderFileFooter(writer *os.File) {
	fmt.Fprintf(writer, "};\n\n/*\n")

	w := tabwriter.NewWriter(writer, 0, 1, 1, ' ', tabwriter.AlignRight)
	func(i ...int64) { fmt.Fprintf(w, "* \t  FreqSource: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(plan.FreqSource))
	func(i ...int64) { fmt.Fprintf(w, "* \tFreqBase: \t%6d + %d/%d\n", i[0], i[1], i[2]) }(To_abc(plan.FreqBase))
	fmt.Fprintf(w, "* \tP: \t%6d\n", plan.P)
	fmt.Fprintf(w, "* \tXtalCL: \t%6d\n* \t\n", plan.XtalCL)
	w.Flush()

	if plan.PLLa != nil {
		plan.PLLa.debug(writer)
	}
	if plan.PLLb != nil {
		plan.PLLb.debug(writer)
	}
	for _, c := range plan.Clocks {
		if c != nil {
			c.debug(writer)
		}
	}
	fmt.Fprintf(writer, " */\n\n#endif\n")
}

func writeHeaderFileRegs(writer *os.File, regs []*reg) {
	for i, r := range regs {
		if r != nil {
			fmt.Fprintf(writer, "\t{ 0x%04X, 0x%02X },\n", i, r.v)
		}
	}
}

func (plan *FrequencyPlan) SetPll(pllNumber PLLNumber, fref *big.Rat, useclkin bool) error {
	var err error
	pll, err := NewPLL(pllNumber, plan.FreqBase, fref, useclkin)
	if pllNumber == PLLA {
		plan.PLLa = pll
	} else {
		plan.PLLb = pll
	}

	return err
}

func (plan *FrequencyPlan) SetClock(clockNumber ClockNumber, pllNumber PLLNumber, fout *big.Rat, idrv IDrv) error {
	var err error
	var clock *Clock
	if pllNumber == PLLA {
		if plan.PLLa == nil {
			return errors.New("PLLA isn't initialized yet")
		}
		clock, err = NewClock(clockNumber, plan.PLLa, fout, idrv)
	} else {
		if plan.PLLb == nil {
			return errors.New("PLLA isn't initialized yet")
		}
		clock, err = NewClock(clockNumber, plan.PLLb, fout, idrv)
	}
	plan.Clocks[clockNumber] = clock
	return err
}

func (plan *FrequencyPlan) fillHead() {
	base := 16
	for i, c := range plan.Clocks {
		if c != nil {
			isInt := byte(0)
			if c.IsIntDivider {
				isInt = byte(1)
			}
			plan.registerMap.head[base+i] = &reg{v: ClockPdnDown | ClockSrcMS | byte(c.PLL.Number)<<5 | isInt<<6 | byte(c.IDrv)}
		} else {
			plan.registerMap.head[base+i] = &reg{v: ClockPdnDown | ClockSrcMS}
		}
	}
}

func (plan *FrequencyPlan) fillMain() {
	if plan.PLLa != nil {
		plan.writePll(plan.PLLa)
	}
	if plan.PLLb != nil {
		plan.writePll(plan.PLLb)
	}
	for _, c := range plan.Clocks {
		if c != nil {
			plan.writeClock(c)
		}
	}
	nrs := []int{0x0095, 0x0096, 0x0097, 0x0098, 0x0099, 0x009A, 0x009B, 0x00A2, 0x00A3, 0x00A4, 0x00A6, 0x00A9, 0x00AA}
	for _, nr := range nrs {
		plan.registerMap.main[nr] = &reg{v: 0}
	}
	plan.registerMap.main[RegXtalCl] = &reg{v: XtalReserved | plan.XtalCL}
	var cval byte
	if plan.registerMap.main[RegPllSrc] == nil {
		cval = 0
	} else {
		cval = plan.registerMap.main[RegPllSrc].v
	}
	plan.registerMap.main[RegPllSrc] = &reg{v: cval | mapClkInDivider(plan.P)}
}

func (plan *FrequencyPlan) writeClock(clock *Clock) {
	if clock.Number < 6 {
		plan.registerMap.main[clock.BaseAddress+0] = &reg{v: byte(clock.Params.MSxP3 >> 8)}
		plan.registerMap.main[clock.BaseAddress+1] = &reg{v: byte(clock.Params.MSxP3 >> 0)}
		lpart := int64(0)
		if clock.Params.DivBy4 {
			lpart = int64(0x3 << 2)
		}
		plan.registerMap.main[clock.BaseAddress+2] = &reg{v: byte(((clock.Params.MSxP1 >> 16) & 0x03) | lpart)}
		plan.registerMap.main[clock.BaseAddress+3] = &reg{v: byte(clock.Params.MSxP1 >> 8)}
		plan.registerMap.main[clock.BaseAddress+4] = &reg{v: byte(clock.Params.MSxP1 >> 0)}
		plan.registerMap.main[clock.BaseAddress+5] = &reg{v: byte((((clock.Params.MSxP3 >> 16) & 0x0f) << 4) | ((clock.Params.MSxP2 >> 16) & 0x0f))}
		plan.registerMap.main[clock.BaseAddress+6] = &reg{v: byte(clock.Params.MSxP2 >> 8)}
		plan.registerMap.main[clock.BaseAddress+7] = &reg{v: byte(clock.Params.MSxP2 >> 0)}
	} else {
		plan.registerMap.main[clock.BaseAddress+0] = &reg{v: byte(clock.Params.MSxP1)}
	}
}

func (plan *FrequencyPlan) writePll(pll *PLL) {
	var cval byte

	if plan.registerMap.main[RegPllSrc] == nil {
		cval = 0
	} else {
		cval = plan.registerMap.main[RegPllSrc].v
	}

	if pll.UseClockIn {
		clksrc := PllBSrcClkIn
		if pll.Number == 0 {
			clksrc = PllASrcClkIn
		}
		plan.registerMap.main[RegPllSrc] = &reg{v: cval | clksrc}
	}

	plan.registerMap.main[pll.BaseAddress+0] = &reg{v: byte(pll.Params.MSNxP3 >> 8)}
	plan.registerMap.main[pll.BaseAddress+1] = &reg{v: byte(pll.Params.MSNxP3 >> 0)}
	plan.registerMap.main[pll.BaseAddress+2] = &reg{v: byte(pll.Params.MSNxP1 >> 16)}
	plan.registerMap.main[pll.BaseAddress+3] = &reg{v: byte(pll.Params.MSNxP1 >> 8)}
	plan.registerMap.main[pll.BaseAddress+4] = &reg{v: byte(pll.Params.MSNxP1 >> 0)}
	plan.registerMap.main[pll.BaseAddress+5] = &reg{v: byte((((pll.Params.MSNxP3 >> 16) & 0x0f) << 4) | ((pll.Params.MSNxP2 >> 16) & 0x0f))}
	plan.registerMap.main[pll.BaseAddress+6] = &reg{v: byte(pll.Params.MSNxP2 >> 8)}
	plan.registerMap.main[pll.BaseAddress+7] = &reg{v: byte(pll.Params.MSNxP2 >> 0)}
}

func (plan *FrequencyPlan) fillTail() {
	base := 16
	for i, c := range plan.Clocks {
		if c != nil {
			isInt := byte(0)
			if c.IsIntDivider {
				isInt = byte(1)
			}
			plan.registerMap.tail[base+i] = &reg{v: ClockPdnUp | ClockSrcMS | byte(c.PLL.Number)<<5 | isInt<<6 | byte(c.IDrv)}
		} else {
			plan.registerMap.tail[base+i] = &reg{v: ClockPdnDown | ClockSrcMS}
		}
	}
}

func LoadPlan(filename string) (*FrequencyPlan, error) {
	var err error
	reader := os.Stdin
	if filename != "" && filename != "-" {
		reader, err = os.Open(filename)

		if err != nil {
			return nil, err
		}
		defer reader.Close()
	}

	result := &FrequencyPlan{}
	yamlDec := yaml.NewDecoder(reader)
	err = yamlDec.Decode(result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func SavePlan(plan *FrequencyPlan, filename string) error {
	var err error
	writer := os.Stdout
	if filename != "" && filename != "-" {
		writer, err = os.OpenFile(filename, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.FileMode(0644))

		if err != nil {
			return err
		}
		defer writer.Close()
	}

	yamlEnc := yaml.NewEncoder(writer)
	defer yamlEnc.Close()

	yamlEnc.SetIndent(2)
	return yamlEnc.Encode(plan)
}

func From_int(a int64) (rat *big.Rat) {
	return big.NewRat(a, 1)
}

func From_abc(a, b, c int64) (rat *big.Rat) {
	return big.NewRat(a*c+b, c)
}

func To_abc(rat *big.Rat) (int64, int64, int64) {
	num := rat.Num().Int64()
	denom := rat.Denom().Int64()

	a := num / denom
	t := new(big.Rat).Sub(rat, big.NewRat(a, 1))
	b := t.Num().Int64()
	c := t.Denom().Int64()

	return a, b, c
}

func Parse_rat(s string) (*big.Rat, error) {
	r1 := new(big.Rat)
	r2 := new(big.Rat)

	_, err := fmt.Sscanf(s, "%f + %f", r1, r2)
	if err != nil {
		_, err = fmt.Sscanf(s, "%f %f", r1, r2)
		if err != nil {
			_, err = fmt.Sscanf(s, "%f", r1)
			if err != nil {
				return nil, err
			}
		}
	}

	return new(big.Rat).Add(r1, r2), nil
}
